import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttertoast/fluttertoast.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Layout',
      theme: ThemeData(
        primarySwatch: Colors.blueGrey,
      ),
      home: const MyHomePage(title: 'Norapich ZA 007 😎'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final myController_user = TextEditingController();
  final myController_pwd = TextEditingController();

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController_user.dispose();
    myController_pwd.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        color: Colors.blueGrey,
        constraints: BoxConstraints.expand(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              CircleAvatar(
                radius: 80.0,
                backgroundImage:
                  NetworkImage('https://media.discordapp.net/attachments/790627573087338518/931469807847620639/Pngtreevector_grade_sheet_icon_3767848.png?width=806&height=806'),
                backgroundColor: Colors.transparent,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: const [
                  Text(
                    '♥',
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.red,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    '♥',
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  // Text(
                  //   'พระมหากษัตย์',
                  //   style: TextStyle(
                  //     fontSize: 18,
                  //     color: Colors.blue,
                  //     fontWeight: FontWeight.bold,
                  //   ),
                  // ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.all(30),
                child: Text(
                  'Grade Panel 💯',
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: TextField(
                  controller: myController_user,
                  decoration: InputDecoration(
                    hintText: 'Required Grade , Recommend A',
                    fillColor: Colors.white,
                    filled: true,
                  ),
                ),
              ),
              // Padding(
              //   padding: const EdgeInsets.only(left: 15, right: 15, bottom: 30),
              //   child: TextField(
              //     controller: myController_pwd,
              //     decoration: InputDecoration(
              //       hintText: 'Your Password',
              //       fillColor: Colors.white,
              //       filled: true,
              //     ),
              //   ),
              // ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton( //Login Button
                    style: ElevatedButton.styleFrom(
                      primary: Colors.lightGreenAccent,
                      onPrimary: Colors.black,
                      padding:
                      EdgeInsets.symmetric(horizontal: 50, vertical: 15),
                      textStyle: TextStyle(fontSize: 20),
                    ),
                    onPressed: () {
                      myController_user.clear();
                      myController_pwd.clear();
                    },
                    child: Text('Submit ‍💻'),
                  ),

                  // ElevatedButton( //Cancle Button
                  //   style: ElevatedButton.styleFrom(
                  //     primary: Colors.red,
                  //     onPrimary: Colors.black,
                  //     padding:
                  //     EdgeInsets.symmetric(horizontal: 30, vertical: 15),
                  //     textStyle: TextStyle(fontSize: 20),
                  //   ),
                  //   onPressed: () => displayToast(),
                  //   //print('Hello Login!'),
                  //   child: Text('Cancle'),
                  // ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  } //end build
  void displayToast() {
    String username = myController_user.text;
    String password = myController_pwd.text;

    Fluttertoast.showToast(
      msg: 'NORAPICH PONHSUWAN ALREADY SUBMIT',
      toastLength: Toast.LENGTH_SHORT,
    );
  }
} //end
